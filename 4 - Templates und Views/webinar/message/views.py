from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from message.models import MessageGroup, Message
from django.contrib import messages
from message.forms import MessageForm

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def add_message(request, group_id = None):
    assert isinstance(request, HttpRequest)
    if request.method == "POST":   

        message_form = MessageForm(request.user, group_id, request.POST)

        if "message_form_submit" in request.POST:
            if message_form.is_valid():
                message_form.save()

                messages.success(request, "Nachricht wurde versendet")

                #Auf die gleiche Seite zurückleiten
                return HttpResponseRedirect('')
            else:
                messages.warning(request, "Nachricht konnte nicht gesendet werden")
            
            print(message_form.fields)

    else:
        # Neue Form erstellen
        if group_id is None:
            message_form = MessageForm(request.user)
        else:
            message_form = MessageForm(request.user, group_id)
        
    return render(            
        request,
        "message/new_message.html",
        {
            "title" : "Neue Nachricht schreiben",
            "message_form" : message_form,
        }        
    )

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def delete_group(request, group_id) :
    assert isinstance(request, HttpRequest)

    message_group_2_delete = get_object_or_404(MessageGroup, pk=group_id)
    if message_group_2_delete.owner == request.user.profile:
        message_group_2_delete.delete()
        messages.success(request, "Gruppe \"" + str(message_group_2_delete) + "\" wurde gelöscht")

    #Auf die Startseite zurückleiten
    return HttpResponseRedirect('/')