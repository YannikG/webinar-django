from django.db import models

# Damit ein User an mehrere Empfänger schreiben kann er eine Gruppe erstellen
# Diese Gruppe kann mehrere Empfänger haben, welche die SMS erhalten
class MessageGroup(models.Model):
    
    # Der Besitzer der Gruppe 
    owner = models.ForeignKey(
        'core.Profile',
        related_name='owner',
        on_delete=models.CASCADE
    )

    # Name
    name = models.CharField(max_length=70, blank=False, null=False)

    # Empfänger der SMS
    receiver = models.ManyToManyField(
        "core.Profile",
        related_name = "receiver_set",
        blank=False
    )

    def __str__(self):
        return "" + self.name

class Message(models.Model):
    send_to = models.ForeignKey(
        'message.MessageGroup',
        related_name='send_to_set',
        on_delete=models.CASCADE
    )
    
    is_send = models.BooleanField(default=False)

    message = models.TextField()
