from django import forms
from message.models import MessageGroup, Message

class AddMessageGroupForm(forms.ModelForm):
    class Meta:
        model = MessageGroup
        fields = ["name", "receiver"]

class MessageForm(forms.Form):
    message = forms.CharField(max_length=155)
    
    def __init__(self, user, pre_selected = None, *args, **kwargs):
        self.user = user
        super(MessageForm, self).__init__(*args, **kwargs)
        
        groups = []
        groups.append((0, "Auswählen"))
        if pre_selected is None:
            groups = tuple([(u'', "Auswählen")] + list([(g.id, str(g)) for g in MessageGroup.objects.filter(owner=user.profile)]))
        else:
            groups = tuple([(g.id, str(g)) for g in MessageGroup.objects.filter(pk=pre_selected, owner=user.profile)])


        self.fields["send_to"] = forms.ChoiceField(label="", choices=groups)

    def save(self):
        if self.is_valid():
            new_message = Message(send_to=MessageGroup.objects.get(pk=self.cleaned_data["send_to"]), message=self.cleaned_data["message"])
            new_message.save()