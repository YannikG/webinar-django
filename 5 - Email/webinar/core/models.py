from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from phonenumber_field.modelfields import PhoneNumberField

# ***
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html
# ***

# Informationen über den User
# Wird automatisch beim erzeugen eines Users erstellt mithilfe von Signals
# Auch das updaten erfolgt über Signals
# Innerhalb des Codes kann eine Phonenumber via request.user.profile.phone z.B. erreicht werden
# Für mehr Informationen bitte den Blogeintrag lesen!

class Profile(models.Model):
    user = models.OneToOneField(
        User, 
        on_delete=models.CASCADE
    )
    phone = PhoneNumberField(blank=True)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return "" + str(self.user.username)

