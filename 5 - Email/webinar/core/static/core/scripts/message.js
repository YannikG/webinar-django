$(".message").each(function(index) {
    color = "black";
    if ($(this).hasClass("success")) {
        color = "green";
    }
    if ($(this).hasClass("error")) {
        color = "red";
    }
    if ($(this).hasClass("warning")) {
        color = "orange";
    }
     Materialize.toast($(this).text() , 4000, color);
});