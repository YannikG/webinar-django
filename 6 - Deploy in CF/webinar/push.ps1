Set-Location "..\.."
echo ">> Please wait until the Script finish!"
$new_req_path = $PSScriptRoot
echo ">> Start copying"
cp "requirements.txt" $new_req_path -Recurse -Force
Set-Location $PSScriptRoot
echo ">> Done copying"
echo ">> Collect Static"
.\..\..\env\Scripts\activate
python manage.py collectstatic --setting=webinar.devsettings --noinput
.\..\..\env\Scripts\deactivate
echo ">> Start Uploading to CF"
echo ">> Make sure you are logged in and selected the corrct Space ond Org!"
echo "Begining..."
cf push -c "./init_db.sh"
echo ">> Done Uploading"
echo ">> Remove requirements.txt from Subproject"
rm "requirements.txt" -Recurse -Force
echo ">> Script done!"