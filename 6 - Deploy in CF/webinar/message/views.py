from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from message.models import MessageGroup, Message
from django.contrib import messages
from message.forms import MessageForm

# Mail
from django.core.mail import EmailMessage

# Settings
from django.conf import settings

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def add_message(request, group_id = None):
    assert isinstance(request, HttpRequest)
    if request.method == "POST":   

        message_form = MessageForm(request.user, group_id, request.POST)

        if "message_form_submit" in request.POST:
            if message_form.is_valid():
                message_group = MessageGroup.objects.get(pk=message_form.cleaned_data["send_to"])
                if len(message_group.receiver.all()) <= settings.EMAIL_MAX_RECEIVER:
                    message_form.save()
                    
                    cleaned_receiver = []
                    for r in message_group.receiver.all():
                        cleaned_r = str(r.phone)

                        # Replace von +41 mit 0 z.B. +417900000000 --> 07900000000
                        cleaned_r = cleaned_r.replace(settings.EMAIL_PHONENUMBER_REMOVE_COUNTRY_CODE, settings.EMAIL_PHONENUMBER_REPLACE_COUNTRY_CODE)
                        cleaned_r += settings.EMAIL_SMS_GATEAWAY
                        cleaned_receiver.append(cleaned_r)
                        
                    msg = EmailMessage("[OR:EasySMS - " + str(message_group) + "]",
                     message_form.cleaned_data["message"],
                     settings.EMAIL_SENDER,
                     cleaned_receiver)
                    msg.send()

                    messages.success(request, "Nachricht wurde versendet")
                else:
                    message.error(request, "Gruppe hat zu viele Empfänger! Maximal " + settings.EMAIL_MAX_RECEIVER + " Empfanger")
                #Auf die gleiche Seite zurückleiten
                return HttpResponseRedirect('')
            else:
                messages.warning(request, "Nachricht konnte nicht gesendet werden")
            
    else:
        # Neue Form erstellen
        if group_id is None:
            message_form = MessageForm(request.user)
        else:
            message_form = MessageForm(request.user, group_id)
        
    return render(            
        request,
        "message/new_message.html",
        {
            "title" : "Neue Nachricht schreiben",
            "message_form" : message_form,
        }        
    )

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def delete_group(request, group_id) :
    assert isinstance(request, HttpRequest)

    message_group_2_delete = get_object_or_404(MessageGroup, pk=group_id)
    if message_group_2_delete.owner == request.user.profile:
        message_group_2_delete.delete()
        messages.success(request, "Gruppe \"" + str(message_group_2_delete) + "\" wurde gelöscht")

    #Auf die Startseite zurückleiten
    return HttpResponseRedirect('/')