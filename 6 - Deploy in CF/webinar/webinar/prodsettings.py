from webinar.basesettings import *
import os, json

VCAP_SERVICES_AsString = os.environ["VCAP_SERVICES"]
# Parse JSON into an object with attributes corresponding to dict keys.
VCAP_SERVICES = json.loads(VCAP_SERVICES_AsString, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': VCAP_SERVICES.mariadbent[0].credentials.name,
        'USER': VCAP_SERVICES.mariadbent[0].credentials.username,
        'PASSWORD': VCAP_SERVICES.mariadbent[0].credentials.password,
        'HOST': VCAP_SERVICES.mariadbent[0].credentials.host,
        'PORT': VCAP_SERVICES.mariadbent[0].credentials.port,
    }
}

DEBUG = False

ALLOWED_HOSTS = ["easysms-webinar.scapp.swisscom.com"]

ROOT_URLCONF = 'webinar.urls'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}

EMAIL_HOST = "sms10.mail2sms.ch"
EMAIL_PORT = 25