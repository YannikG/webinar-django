from webinar.basesettings import *

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'webinar',
        'USER': 'root',
        'PASSWORD': 'gibbiX12345',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True