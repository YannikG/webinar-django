#!/bin/sh
echo "------ Make migrations ------"
python manage.py makemigrations --noinput

echo "------ Create database tables ------"
# python manage.py migrate core --noinput --fake
python manage.py migrate --noinput
 
echo "------ create default admin user ------"
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@webinar.intra', 'gibbiX12345')" | python manage.py shell

echo "------ starting gunicorn ------"
gunicorn webinar.wsgi --workers 10
