from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User

# Create your views here.
def home(request) :
    assert isinstance(request, HttpRequest)
    return render(            
        request,
        "core/home.html",
        {
            "title" : "Startseite",
            "users" : User.objects.all()
        }        
    )