# README #

Dies ist das Repo zum Webseminar �ber Django f�r Swisscom NEX

### Setup ###

1. Clonen
2. Virtual Env erstellen
3. Virtual Env aktivieren
5. Mit Pip die Pakete installieren
6. python manage.py runserver --setting=settings.development ausf�hren
7. Kaffee holen

### Bei Issues ###
Bei Fehler bitte via Slack oder Skype anschreiben.

### Was darf ich mit dem Code anstellen ###
Dieses Projekt darf kopiert, geteilt und als Template verwendet werden, da dies eine Hilfestellung zu Django ist.