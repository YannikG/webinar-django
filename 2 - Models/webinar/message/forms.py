from django import forms
from message.models import MessageGroup

class AddMessageGroupForm(forms.ModelForm):
    class Meta:
        model = MessageGroup
        fields = ["name", "receiver"]
