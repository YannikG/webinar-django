from django import forms
from core.models import Profile

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["phone",]