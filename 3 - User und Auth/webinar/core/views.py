from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User
from message.models import MessageGroup
from message.forms import AddMessageGroupForm
from core.forms import EditProfileForm
from django.contrib import messages

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def home(request) :
    
    assert isinstance(request, HttpRequest)

    if request.method == "POST":   

        edit_profile_form = EditProfileForm(request.POST)
        add_message_group_form = AddMessageGroupForm(request.POST)

        if "edit_profile_submit" in request.POST:
            if edit_profile_form.is_valid():
                edit_profile = edit_profile_form.save(commit=False)
                request.user.profile.phone = edit_profile.phone
                request.user.profile.save()

                messages.success(request, "Profil wurde angepasst")

                #Auf die gleiche Seite zurückleiten
                return HttpResponseRedirect('')
            else:
                messages.warning(request, "Profil konnte nicht gespeichert werden")

        elif "add_message_group_submit" in request.POST:
            if add_message_group_form.is_valid():
                # Neue Gruppe wird erstellt aber nicht direkt gespeichert
                new_group = add_message_group_form.save(commit=False)

                # Neuer Gruppe einen Owner geben (Current User)
                new_group.owner = request.user.profile

                # Neue Gruppe erstellen (in DB speichern)
                new_group.save()

                # Many to Many Daten Speichern (Empfänger)
                add_message_group_form.save_m2m()

                messages.success(request, "Gruppe \"" + str(new_group) + "\" wurde erstellt")

                #Auf die gleiche Seite zurückleiten
                return HttpResponseRedirect('')
            else:
                messages.warning(request, "Gruppe konnte nicht gespeichert werden")

    else:
        # Neue Form erstellen
        add_message_group_form = AddMessageGroupForm()
        edit_profile_form = EditProfileForm(instance=request.user.profile)

        
    return render(            
        request,
        "core/home.html",
        {
            "title" : "Startseite",
            "users" : User.objects.all(),
            "message_group" : MessageGroup.objects.all(),
            "add_message_group_form" : add_message_group_form,
            "edit_profile_form" : edit_profile_form,
        }        
    )

