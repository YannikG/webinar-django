from django.shortcuts import render
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from message.models import MessageGroup
from django.contrib import messages

@login_required # Der Benutzer muss angemeldet sein damit er diese Seite öffnen kann
def delete_group(request, message_id) :
    assert isinstance(request, HttpRequest)

    message_group_2_delete = get_object_or_404(MessageGroup, pk=message_id)
    if message_group_2_delete.owner == request.user.profile:
        message_group_2_delete.delete()
        messages.success(request, "Gruppe \"" + str(message_group_2_delete) + "\" wurde gelöscht")

    #Auf die Startseite zurückleiten
    return HttpResponseRedirect('/')